/**
 * Automatically generated file. DO NOT MODIFY
 */
package in.infinitycircle.olii;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "in.infinitycircle.olii";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 11;
  public static final String VERSION_NAME = "1.1";
}
