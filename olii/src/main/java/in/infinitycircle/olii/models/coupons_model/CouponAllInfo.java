
package in.infinitycircle.olii.models.coupons_model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CouponAllInfo implements Parcelable {

    @SerializedName("coupans_id")
    @Expose
    private String coupansId;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("discount")
    @Expose
    private String discount;
    @SerializedName("discount_type")
    @Expose
    private String discountType;
    @SerializedName("date_created")
    @Expose
    private String dateCreated;
    @SerializedName("date_modified")
    @Expose
    private String dateModified;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("expiry_date")
    @Expose
    private String expiryDate;
    @SerializedName("usage_count")
    @Expose
    private String usageCount;
    @SerializedName("individual_use")
    @Expose
    private String individualUse;
    @SerializedName("usage_limit")
    @Expose
    private String usageLimit;
    @SerializedName("usage_limit_per_user")
    @Expose
    private String usageLimitPerUser;
    @SerializedName("limit_usage_to_x_items")
    @Expose
    private String limitUsageToXItems;
    @SerializedName("free_shipping")
    @Expose
    private String freeShipping;
    
    @SerializedName("exclude_sale_items")
    @Expose
    private String excludeSaleItems;
    @SerializedName("minimum_amount")
    @Expose
    private String minimumAmount;
    @SerializedName("maximum_amount")
    @Expose
    private String maximumAmount;
    



    public CouponAllInfo() {
    }


    public String getCoupansId() {
        return coupansId;
    }

    public void setCoupansId(String coupansId) {
        this.coupansId = coupansId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getDateModified() {
        return dateModified;
    }

    public void setDateModified(String dateModified) {
        this.dateModified = dateModified;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDiscountType() {
        return discountType;
    }

    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
    
    public String getDiscount() {
        return discount;
    }
    
    public void setDiscount(String discount) {
        this.discount = discount;
    }
    
    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getUsageCount() {
        return usageCount;
    }

    public void setUsageCount(String usageCount) {
        this.usageCount = usageCount;
    }

    public String getIndividualUse() {
        return individualUse;
    }

    public void setIndividualUse(String individualUse) {
        this.individualUse = individualUse;
    }

    public String getUsageLimit() {
        return usageLimit;
    }

    public void setUsageLimit(String usageLimit) {
        this.usageLimit = usageLimit;
    }

    public String getUsageLimitPerUser() {
        return usageLimitPerUser;
    }

    public void setUsageLimitPerUser(String usageLimitPerUser) {
        this.usageLimitPerUser = usageLimitPerUser;
    }

    public String getLimitUsageToXItems() {
        return limitUsageToXItems;
    }

    public void setLimitUsageToXItems(String limitUsageToXItems) {
        this.limitUsageToXItems = limitUsageToXItems;
    }

    public String getFreeShipping() {
        return freeShipping;
    }

    public void setFreeShipping(String freeShipping) {
        this.freeShipping = freeShipping;
    }

    public String getExcludeSaleItems() {
        return excludeSaleItems;
    }

    public void setExcludeSaleItems(String excludeSaleItems) {
        this.excludeSaleItems = excludeSaleItems;
    }

    public String getMinimumAmount() {
        return minimumAmount;
    }

    public void setMinimumAmount(String minimumAmount) {
        this.minimumAmount = minimumAmount;
    }

    public String getMaximumAmount() {
        return maximumAmount;
    }

    public void setMaximumAmount(String maximumAmount) {
        this.maximumAmount = maximumAmount;
    }




    //********** Describes the kinds of Special Objects contained in this Parcelable Instance's marshaled representation *********//

    @Override
    public int describeContents() {
        return 0;
    }



    //********** Writes the values to the Parcel *********//

    public void writeToParcel(Parcel parcel_out, int flags) {
        parcel_out.writeValue(coupansId);
        parcel_out.writeValue(code);
        parcel_out.writeValue(dateCreated);
        parcel_out.writeValue(dateModified);
        parcel_out.writeValue(description);
        parcel_out.writeValue(discountType);
        parcel_out.writeValue(amount);
        parcel_out.writeValue(discount);
        parcel_out.writeValue(expiryDate);
        parcel_out.writeValue(usageCount);
        parcel_out.writeValue(individualUse);
        parcel_out.writeValue(usageLimit);
        parcel_out.writeValue(usageLimitPerUser);
        parcel_out.writeValue(limitUsageToXItems);
        parcel_out.writeValue(freeShipping);
        parcel_out.writeValue(excludeSaleItems);
        parcel_out.writeValue(minimumAmount);
        parcel_out.writeValue(maximumAmount);
    }



    //********** Generates Instances of Parcelable class from a Parcel *********//

    public static final Creator<CouponAllInfo> CREATOR = new Creator<CouponAllInfo>() {

        // Creates a new Instance of the Parcelable class, Instantiating it from the given Parcel
        @Override
        public CouponAllInfo createFromParcel(Parcel parcel_in) {
            return new CouponAllInfo(parcel_in);
        }

        // Creates a new array of the Parcelable class
        @Override
        public CouponAllInfo[] newArray(int size) {
            return new CouponAllInfo[size];
        }
    };



    //********** Retrieves the values from the Parcel *********//

    protected CouponAllInfo(Parcel parcel_in) {
        this.coupansId = parcel_in.readString();
        this.code = parcel_in.readString();
        this.dateCreated = parcel_in.readString();
        this.dateModified = parcel_in.readString();
        this.description = parcel_in.readString();
        this.discountType = parcel_in.readString();
        this.amount = parcel_in.readString();
        this.discount = parcel_in.readString();
        this.expiryDate = parcel_in.readString();
        this.usageCount = parcel_in.readString();
        this.individualUse = parcel_in.readString();
        this.usageLimit = parcel_in.readString();
        this.usageLimitPerUser = parcel_in.readString();
        this.limitUsageToXItems = parcel_in.readString();
        this.freeShipping = parcel_in.readString();
        this.excludeSaleItems = parcel_in.readString();
        this.minimumAmount = parcel_in.readString();
        this.maximumAmount = parcel_in.readString();
    }

}
