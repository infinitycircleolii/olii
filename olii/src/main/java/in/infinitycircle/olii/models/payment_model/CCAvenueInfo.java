
package in.infinitycircle.olii.models.payment_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class CCAvenueInfo {
    
    @SerializedName("command")
    @Expose
    private String command;
    @SerializedName("access_code")
    @Expose
    private String accessCode;
    @SerializedName("merchant_id")
    @Expose
    private String merchantId;
    @SerializedName("order_id")
    @Expose
    private String orderId;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("redirect_url")
    @Expose
    private String redirectUrl;
    @SerializedName("cancel_url")
    @Expose
    private String cancelUrl;
    
    public void setCommand(String command) {
        this.command = command;
    }

    public void setAccessCode(String accessCode) {
        this.accessCode = accessCode;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    public void setCancelUrl(String cancelUrl) {
        this.cancelUrl = cancelUrl;
    }

}

