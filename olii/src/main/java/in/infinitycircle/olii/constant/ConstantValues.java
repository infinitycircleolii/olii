package in.infinitycircle.olii.constant;


/**
 * ConstantValues contains some constant variables, used all over the App.
 **/


public class ConstantValues {
    
    
    //*********** API Base URL ********//
    public static final String ECOMMERCE_URL = "http://infinitycircle.in/shop/";
    
    public static final String ECOMMERCE_CONSUMER_KEY = "858846f51570128973d0a8410a";
    public static final String ECOMMERCE_CONSUMER_SECRET = "5f53b3e115701289734476f1a7";
    
    
    //*********** API Base URL ********//
  /*  public static final String ECOMMERCE_URL = "http://demo2.androidecommerce.com/";
    
    public static final String ECOMMERCE_CONSUMER_KEY = "84afd11a15263862148aa0178f";
    public static final String ECOMMERCE_CONSUMER_SECRET = "98809ad51526386214830504e6";*/
    
    public static final String CODE_VERSION = "1.0";
    
    public static final boolean IS_CLIENT_ACTIVE = false;                               // "false" if compiling the project for Demo, "true" otherwise
    
    public static final String DEFAULT_NOTIFICATION = "onesignal";                      // "fcm" for FCM_Notifications, "onesignal" for OneSignal
    
    public static String APP_HEADER;
    
    public static String DEFAULT_HOME_STYLE;
    public static String DEFAULT_CATEGORY_STYLE;
    
    public static int LANGUAGE_ID;
    public static String LANGUAGE_CODE;
    public static String CURRENCY_SYMBOL;
    public static long NEW_PRODUCT_DURATION;
    
    public static boolean IS_GOOGLE_LOGIN_ENABLED;
    public static boolean IS_FACEBOOK_LOGIN_ENABLED;
    public static boolean IS_ADD_TO_CART_BUTTON_ENABLED;
    
    public static boolean IS_ADMOBE_ENABLED;
    public static String ADMOBE_ID;
    public static String AD_UNIT_ID_BANNER;
    public static String AD_UNIT_ID_INTERSTITIAL;
    
    public static String ABOUT_US;
    public static String TERMS_SERVICES;
    public static String PRIVACY_POLICY;
    public static String REFUND_POLICY;
    
    public static boolean IS_USER_LOGGED_IN;
    public static boolean IS_PUSH_NOTIFICATIONS_ENABLED;
    public static boolean IS_LOCAL_NOTIFICATIONS_ENABLED;
    
    
    
    
}
