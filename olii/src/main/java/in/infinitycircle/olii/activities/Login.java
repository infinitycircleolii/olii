package in.infinitycircle.olii.activities;


import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.os.Bundle;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.Toolbar;

import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.gson.Gson;

import in.infinitycircle.olii.R;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import in.infinitycircle.olii.network.APIClient;
import in.infinitycircle.olii.customs.DialogLoader;
import in.infinitycircle.olii.network.StartAppRequests;
import in.infinitycircle.olii.utils.LocaleHelper;
import in.infinitycircle.olii.utils.ValidateInputs;
import in.infinitycircle.olii.app.MyAppPrefsManager;
import in.infinitycircle.olii.databases.User_Info_DB;
import in.infinitycircle.olii.constant.ConstantValues;
import in.infinitycircle.olii.models.user_model.UserData;
import in.infinitycircle.olii.models.user_model.UserDetails;
import retrofit2.http.Url;


/**
 * Login activity handles User's Email, Facebook and Google Login
 **/


public class Login extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    View parentView;
    Toolbar toolbar;
    ActionBar actionBar;

    EditText user_email, user_password;
    TextView forgotPasswordText, signupText;
    Button loginBtn, facebookLoginBtn, googleLoginBtn;

    User_Info_DB userInfoDB;
    DialogLoader dialogLoader;
    SharedPreferences.Editor editor;
    SharedPreferences sharedPreferences;
    private CallbackManager callbackManager;
    private GoogleSignInOptions mGoogleSignInOptions;
    private int RC_SIGN_IN = 1;
    private AppCompatButton appCompatButton;
    private GoogleSignInClient mGoogleSignInClient;
    private String TAG = "Login";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        // Initialize Facebook SDk for Facebook Login
        FacebookSdk.sdkInitialize(getApplicationContext());

        // Initializing Google SDK for Google Login
        appCompatButton = findViewById(R.id.googleLoginBtn);
        mGoogleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, mGoogleSignInOptions);

        appCompatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn();
            }
        });

        // setting Toolbar
        toolbar = (Toolbar) findViewById(R.id.myToolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setTitle(getString(R.string.login));
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(false);

        // Binding Layout Views
        user_email = (EditText) findViewById(R.id.user_email);
        user_password = (EditText) findViewById(R.id.user_password);
        loginBtn = (Button) findViewById(R.id.loginBtn);
        facebookLoginBtn = (Button) findViewById(R.id.facebookLoginBtn);
        googleLoginBtn = (Button) findViewById(R.id.googleLoginBtn);
        signupText = (TextView) findViewById(R.id.login_signupText);
        forgotPasswordText = (TextView) findViewById(R.id.forgot_password_text);
        parentView = signupText;

        if (ConstantValues.IS_GOOGLE_LOGIN_ENABLED) {
            appCompatButton.setVisibility(View.VISIBLE);
        } else {
            appCompatButton.setVisibility(View.GONE);
        }

        if (ConstantValues.IS_FACEBOOK_LOGIN_ENABLED) {
            facebookLoginBtn.setVisibility(View.VISIBLE);
        } else {
            facebookLoginBtn.setVisibility(View.GONE);
        }

        dialogLoader = new DialogLoader(Login.this);

        userInfoDB = new User_Info_DB();
        sharedPreferences = getSharedPreferences("UserInfo", MODE_PRIVATE);

        user_email.setText(sharedPreferences.getString("userEmail", null));

        // Register Callback for Facebook LoginManager
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // Get Access Token and proceed Facebook Registration
                String accessToken = loginResult.getAccessToken().getToken();
                Log.e("Facebook Login result", new Gson().toJson(loginResult));
                processFacebookRegistration(accessToken);
            }

            @Override
            public void onCancel() {
                // If User Canceled
            }

            @Override
            public void onError(FacebookException e) {
                // If Login Fails
                Toast.makeText(Login.this, "FacebookException : " + e, Toast.LENGTH_LONG).show();
            }
        });

        // Handle on Forgot Password Click
        forgotPasswordText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                AlertDialog.Builder dialog = new AlertDialog.Builder(Login.this);
                View dialogView = getLayoutInflater().inflate(R.layout.dialog_input, null);
                dialog.setView(dialogView);
                dialog.setCancelable(true);
                final Button dialog_button = (Button) dialogView.findViewById(R.id.dialog_button);
                final EditText dialog_input = (EditText) dialogView.findViewById(R.id.dialog_input);
                final TextView dialog_title = (TextView) dialogView.findViewById(R.id.dialog_title);
                dialog_button.setText(getString(R.string.send));
                dialog_title.setText(getString(R.string.forgot_your_password));
                final AlertDialog alertDialog = dialog.create();
                alertDialog.show();
                dialog_button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (ValidateInputs.isValidEmail(dialog_input.getText().toString().trim())) {
                            // Request for Password Reset
                            processForgotPassword(dialog_input.getText().toString());
                        } else {
                            Snackbar.make(parentView, getString(R.string.invalid_email), Snackbar.LENGTH_LONG).show();
                        }
                        alertDialog.dismiss();
                    }
                });
            }
        });


        signupText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Navigate to SignUp Activity
                startActivity(new Intent(Login.this, Signup.class));
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_out_left);
            }
        });


        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Validate Login Form Inputs
                boolean isValidData = validateLogin();
                if (isValidData) {
                    // Proceed User Login
                    processLogin();
                }
            }
        });


        // Handle Facebook Login Button click
        facebookLoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Logout the User if already Logged-in
                if (AccessToken.getCurrentAccessToken() != null) {
                    LoginManager.getInstance().logOut();
                }
                // Login and Access User Date
                LoginManager.getInstance().logInWithReadPermissions(Login.this, Arrays.asList("public_profile", "email"));
            }
        });
        getHashKey();

    }

    private void signIn() {
        Intent signInIntent;
        signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleGoogleSignInResult(task);
        }
    }

    private void handleGoogleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount acc = completedTask.getResult(ApiException.class);
            processGoogleRegistration(acc);
        } catch (ApiException e) {
            Toast.makeText(Login.this, "Login Failed", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    //*********** Called if Connection fails for Google Login ********//

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        // If Connection fails for GoogleApiClient
    }


    //*********** Receives the result from a previous call of startActivityForResult(Intent, int) ********//


    private void getHashKey() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    getPackageName(),
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException | NoSuchAlgorithmException e) {

        }

    }

    //*********** Proceed Login with User Email and Password ********//

    private void processLogin() {
        dialogLoader.showProgressDialog();
        Call<UserData> call = APIClient.getInstance().processLogin(user_email.getText().toString().trim(), user_password.getText().toString().trim());
        call.enqueue(new Callback<UserData>() {
            @Override
            public void onResponse(Call<UserData> call, Response<UserData> response) {

                dialogLoader.hideProgressDialog();

                if (response.isSuccessful()) {

                    if (response.body().getSuccess().equalsIgnoreCase("1") || response.body().getSuccess().equalsIgnoreCase("2")) {
                        // Get the User Details from Response
                        UserDetails userDetails = response.body().getData().get(0);

                        // Save User Data to Local Databases
                        if (userInfoDB.getUserData(userDetails.getCustomersId()) != null) {
                            // User already exists
                            userInfoDB.updateUserData(userDetails);
                        } else {
                            // Insert Details of New User
                            userInfoDB.insertUserData(userDetails);
                        }

                        // Save necessary details in SharedPrefs
                        editor = sharedPreferences.edit();
                        editor.putString("userID", userDetails.getCustomersId());
                        editor.putString("userEmail", userDetails.getCustomersEmailAddress());
                        editor.putString("userName", userDetails.getCustomersFirstname() + " " + userDetails.getCustomersLastname());
                        editor.putString("userDefaultAddressID", userDetails.getCustomersDefaultAddressId());
                        editor.putBoolean("isLogged_in", true);
                        editor.apply();

                        // Set UserLoggedIn in MyAppPrefsManager
                        MyAppPrefsManager myAppPrefsManager = new MyAppPrefsManager(Login.this);
                        myAppPrefsManager.setUserLoggedIn(true);

                        // Set isLogged_in of ConstantValues
                        ConstantValues.IS_USER_LOGGED_IN = myAppPrefsManager.isUserLoggedIn();


                        StartAppRequests.RegisterDeviceForFCM(Login.this);


                        // Navigate back to MainActivity
                        Intent i = new Intent(Login.this, MainActivity.class);
                        startActivity(i);
                        finish();
                        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_out_right);

                    } else if (response.body().getSuccess().equalsIgnoreCase("0")) {
                        // Get the Error Message from Response
                        String message = response.body().getMessage();
                        Snackbar.make(parentView, message, Snackbar.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(Login.this, getString(R.string.unexpected_response), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    // Show the Error Message
                    Toast.makeText(Login.this, response.message(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<UserData> call, Throwable t) {
                dialogLoader.hideProgressDialog();
                Toast.makeText(Login.this, "NetworkCallFailure : " + t, Toast.LENGTH_LONG).show();
            }
        });
    }


    //*********** Proceed Forgot Password Request ********//

    private void processForgotPassword(String email) {

        dialogLoader.showProgressDialog();

        Call<UserData> call = APIClient.getInstance()
                .processForgotPassword
                        (
                                email
                        );

        call.enqueue(new Callback<UserData>() {
            @Override
            public void onResponse(Call<UserData> call, Response<UserData> response) {

                dialogLoader.hideProgressDialog();

                if (response.isSuccessful()) {
                    // Show the Response Message
                    String message = response.body().getMessage();
                    Snackbar.make(parentView, message, Snackbar.LENGTH_LONG).show();

                } else {
                    // Show the Error Message
                    Toast.makeText(Login.this, response.message(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<UserData> call, Throwable t) {
                dialogLoader.hideProgressDialog();
                Toast.makeText(Login.this, "NetworkCallFailure : " + t, Toast.LENGTH_LONG).show();
            }
        });
    }


    //*********** Proceed Facebook Registration Request ********//

    private void processFacebookRegistration(String access_token) {
        dialogLoader.showProgressDialog();
        Log.e("keyh", access_token);
        Call<UserData> call = APIClient.getInstance().facebookRegistration(access_token);
        call.enqueue(new Callback<UserData>() {
            @Override
            public void onResponse(Call<UserData> call, Response<UserData> response) {

                dialogLoader.hideProgressDialog();

                if (response.isSuccessful()) {
                    if (response.body().getSuccess().equalsIgnoreCase("1") || response.body().getSuccess().equalsIgnoreCase("2")) {
                        // Get the User Details from Response
                        UserDetails userDetails = response.body().getData().get(0);

                        // Save User Data to Local Databases
                        if (userInfoDB.getUserData(userDetails.getCustomersId()) != null) {
                            // User already exists
                            userInfoDB.updateUserData(userDetails);
                        } else {
                            // Insert Details of New User
                            userInfoDB.insertUserData(userDetails);
                        }

                        // Save necessary details in SharedPrefs
                        editor = sharedPreferences.edit();
                        editor.putString("userID", userDetails.getCustomersId());
                        editor.putString("userEmail", userDetails.getCustomersEmailAddress());
                        editor.putString("userName", userDetails.getCustomersFirstname() + " " + userDetails.getCustomersLastname());
                        editor.putString("userDefaultAddressID", userDetails.getCustomersDefaultAddressId());
                        editor.putBoolean("isLogged_in", true);
                        editor.apply();


                        // Set UserLoggedIn in MyAppPrefsManager
                        MyAppPrefsManager myAppPrefsManager = new MyAppPrefsManager(Login.this);
                        myAppPrefsManager.setUserLoggedIn(true);

                        // Set isLogged_in of ConstantValues
                        ConstantValues.IS_USER_LOGGED_IN = myAppPrefsManager.isUserLoggedIn();

                        StartAppRequests.RegisterDeviceForFCM(Login.this);


                        // Navigate back to MainActivity
                        Intent i = new Intent(Login.this, MainActivity.class);
                        startActivity(i);
                        finish();
                        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_out_right);

                    } else if (response.body().getSuccess().equalsIgnoreCase("0")) {
                        // Get the Error Message from Response
                        String message = response.body().getMessage();
                        Snackbar.make(parentView, message, Snackbar.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(Login.this, getString(R.string.unexpected_response), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    // Show the Error Message
                    Log.e("keyhE", "Error");
                    Toast.makeText(Login.this, response.message(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<UserData> call, Throwable t) {
                dialogLoader.hideProgressDialog();
                Toast.makeText(Login.this, "NetworkCallFailure : " + t, Toast.LENGTH_LONG).show();
            }
        });
    }

    //*********** Proceed Google Registration Request ********//

    private void processGoogleRegistration(GoogleSignInAccount account) {

        dialogLoader.showProgressDialog();
        Toast.makeText(Login.this,  "Welcome "+ account.getGivenName() + " "+account.getFamilyName(), Toast.LENGTH_SHORT).show();

        String photoURL = account.getPhotoUrl() != null ? account.getPhotoUrl().toString() : "";

        Call<UserData> call = APIClient.getInstance().googleRegistration(
                                account.getIdToken(),
                                account.getId(),
                                account.getGivenName(),
                                account.getFamilyName(),
                                account.getEmail(),
                                photoURL);

        call.enqueue(new Callback<UserData>() {
            @Override
            public void onResponse(Call<UserData> call, Response<UserData> response) {

                dialogLoader.hideProgressDialog();

                if (response.isSuccessful()) {
                    if (response.body().getSuccess().equalsIgnoreCase("1") || response.body().getSuccess().equalsIgnoreCase("2")) {

                        UserDetails userDetails = response.body().getData().get(0);

                        // Save User Data to Local Databases
                        if (userInfoDB.getUserData(userDetails.getCustomersId()) != null) {
                            userInfoDB.updateUserData(userDetails);
                        } else {
                            userInfoDB.insertUserData(userDetails);
                        }

                        // Save necessary details in SharedPrefs
                        editor = sharedPreferences.edit();
                        editor.putString("userID", userDetails.getCustomersId());
                        editor.putString("userEmail", userDetails.getCustomersEmailAddress());
                        editor.putString("userName", userDetails.getCustomersFirstname() + " " + userDetails.getCustomersLastname());
                        editor.putString("userDefaultAddressID", userDetails.getCustomersDefaultAddressId());
                        editor.apply();


                        // Set UserLoggedIn in MyAppPrefsManager
                        MyAppPrefsManager myAppPrefsManager = new MyAppPrefsManager(Login.this);
                        myAppPrefsManager.setUserLoggedIn(true);

                        ConstantValues.IS_USER_LOGGED_IN = myAppPrefsManager.isUserLoggedIn();

                        StartAppRequests.RegisterDeviceForFCM(Login.this);


                        // Navigate back to MainActivity
                        startActivity(new Intent(Login.this, MainActivity.class));
                        finish();
                        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_out_right);


                    } else if (response.body().getSuccess().equalsIgnoreCase("0")) {
                        Snackbar.make(parentView, response.body().getMessage(), Snackbar.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(Login.this, getString(R.string.unexpected_response), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    // Show the Error Message
                    Toast.makeText(Login.this, response.message(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<UserData> call, Throwable t) {
                dialogLoader.hideProgressDialog();
                Toast.makeText(Login.this, "NetworkCallFailure : " + t, Toast.LENGTH_LONG).show();
            }
        });
    }


    //*********** Validate Login Form Inputs ********//

    private boolean validateLogin() {
        if (!ValidateInputs.isValidEmail(user_email.getText().toString().trim())) {
            user_email.setError(getString(R.string.invalid_email));
            return false;
        } else if (!ValidateInputs.isValidPassword(user_password.getText().toString().trim())) {
            user_password.setError(getString(R.string.invalid_password));
            return false;
        } else {
            return true;
        }
    }


    //*********** Set the Base Context for the ContextWrapper ********//

    @Override
    protected void attachBaseContext(Context newBase) {

        String languageCode = ConstantValues.LANGUAGE_CODE;
        if ("".equalsIgnoreCase(languageCode))
            languageCode = ConstantValues.LANGUAGE_CODE = "en";

        super.attachBaseContext(LocaleHelper.wrapLocale(newBase, languageCode));
    }


    //*********** Called when the Activity has detected the User pressed the Back key ********//

    @Override
    public void onBackPressed() {

        // Navigate back to MainActivity
        startActivity(new Intent(Login.this, MainActivity.class));
        finish();
        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_out_right);
    }

}

